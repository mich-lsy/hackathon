# Description of Library Hackathon implementation

> Design pattern, design choices, features

- Polymorphism: Library keeps track of an ArrayList of LibraryItems. LibraryItems is the superclass of all items that can be found in the library
- Encapsulation: All instance variables are marked private and have getters & setters 
- Design choice: Borrowable is an interface. This is used to ensure only borrowable items can be checked out. 
- Design pattern: Made use of the factory design pattern to create the library items in main(). 