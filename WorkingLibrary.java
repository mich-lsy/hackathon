import java.util.Date;

/***
 * 
 * will demonstrate the ability to borrow, add, and remove books and the various
 * other features of your library.
 */

public class WorkingLibrary {

    public static void main(String[] args) {
        Library demoLib = new Library(); 
        LibraryItemFactory demoFactory = new LibraryItemFactory();
        LibraryItems a1 = demoFactory.getItem("BOOK", "Adventure in Time", "Bob");
        LibraryItems a2 = demoFactory.getItem("BOOK", "PeterPan", "Dylan");
        LibraryItems a3 = demoFactory.getItem("CDS", "Little Piece of Heaven", "Avenged Sevenfold");
        LibraryItems a4 = demoFactory.getItem("PERIODICALS", "Summer 2020", "Vogue");
        System.out.println("Adding items----------");
        demoLib.addItem(a1);
        demoLib.addItem(a2);
        demoLib.addItem(a3);
        demoLib.addItem(a4);
        demoLib.displayAll();
        System.out.println("Removing items----------");
        demoLib.removeItem(a2);
        demoLib.displayAll();
        System.out.println("Borrow--------------");
        demoLib.borrow(a1, "Michelle", new Date() );
        demoLib.borrow(a3, "Michelle", new Date() );
        demoLib.borrow(a4, "Michelle", new Date() );
        demoLib.displayAll();
        System.out.println("Return---------------");
        demoLib.returnItem(a1);
        demoLib.displayAll();
    }
    
}