import java.util.Date;

public interface Borrowable {
    public void setBorrower(String borrowerName);
    public String getBorrower();
    public void setDueDate(Date dueDate);
    public Date getDueDate();
}