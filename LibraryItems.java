/*** Parent class for all items that can be added/removed into the library */

public abstract class LibraryItems {
    private String itemName;
    private String creatorName;

	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}


}