import java.util.ArrayList;
import java.util.Date;

/***
 * Users to have items added to it and items removed from it. It will also be
 * possible to borrow items from the library.
 * 
 * Do not create more than 6 items of each type in your library to demonstrate a
 * working example.
 */

 public class Library{
     private ArrayList<LibraryItems> inLibrary; //Lists all library items 
     private ArrayList<Borrowable> borrowedItems; //Lists all borrowed library items 
     
     public Library(){
         inLibrary = new ArrayList<LibraryItems>();
         borrowedItems = new ArrayList<Borrowable>();
     }

    public void addItem(LibraryItems newItem){
        inLibrary.add(newItem);
    }

    // Returns true if item was able to be removed
    public boolean removeItem(LibraryItems itemRemove){
        for(LibraryItems item: this.inLibrary){
            if (item.hashCode() == itemRemove.hashCode()){
                inLibrary.remove(item);
                System.out.println("Removed item title: " + itemRemove.getItemName());
                return true;
            }
        }
        return false;
    }

    // Used for debugging. Prints out all library items & borrowed items 
    public void displayAll(){
        for(LibraryItems item: this.inLibrary){
            System.out.println("In Library Item name: " + item.getItemName() + ", HashCode: " + item.hashCode());
        }
        for(Borrowable item: this.borrowedItems){
            LibraryItems temp = (LibraryItems) item;
            System.out.println("In Library Checkout list Item name: " + temp.getItemName() + ", HashCode: " + temp.hashCode() );
        }
    }

    //Borrows an item 
    //Returns true if successful, else returns false 
    public boolean borrow(LibraryItems itemBorrow, String borrower, Date dueDate){
        for(LibraryItems item: this.inLibrary){
            if (item.hashCode() == itemBorrow.hashCode()){
                if (item instanceof Borrowable){
                    inLibrary.remove(item);
                    Borrowable checkout = (Borrowable) item;
                    checkout.setBorrower(borrower);
                    checkout.setDueDate(dueDate);
                    borrowedItems.add(checkout);
                    System.out.println("Checked out book title: " + item.getItemName());
                    return true;
                }
            }
        }
        return false;
    }

    //Returns true if checkIn item was actually checkedOut from this library, else false 
    public boolean returnItem(LibraryItems checkIn){
        for(Borrowable item: this.borrowedItems){
            if ((checkIn instanceof Borrowable) && (item.hashCode() == checkIn.hashCode())){
                borrowedItems.remove(item);
                inLibrary.add(checkIn);
                System.out.println("Checked in book title: " + checkIn.getItemName());
                return true;
            }
        }
        return false;

    }
 }