public class LibraryItemFactory {
    public LibraryItems getItem(String type, String itemName, String itemCreator){
        if (type.equalsIgnoreCase("BOOK")){
            return new Book(itemName, itemCreator);
        } else if (type.equalsIgnoreCase("CDS")){
            return new Cds(itemName, itemCreator);
        } else if (type.equalsIgnoreCase("PERIODICALS")){
            return new Periodicals(itemName, itemCreator);
        } else {
            return null;
        }
    }
    
}