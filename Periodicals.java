
/***
 * Periodicals cannot be borrowed. 
 */
public class Periodicals extends LibraryItems {
    public Periodicals(String name, String creatorName){
        super.setCreatorName(creatorName);
        super.setItemName(name);
    }
    
}