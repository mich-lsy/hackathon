import java.util.Date;

public class Book extends LibraryItems implements Borrowable {
    private String borrower; 
    private Date dueDate; 

    public Book(String name, String creatorName) {
        super.setCreatorName(creatorName);
        super.setItemName(name);
    }

    @Override
    public String getBorrower() {
        return this.borrower;
    }

    @Override
    public Date getDueDate() {
        return this.dueDate;
    }

    @Override
    public void setBorrower(String borrowerName) {
        this.borrower = borrowerName;

    }

    @Override
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;

    }
}